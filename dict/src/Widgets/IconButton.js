/**
 * Created by prg-15 on 12.03.2018.
 */
import React from 'react';

export default function IconButton(props){
	return(
	  <button className="icon-button">
		  <img src={props.src}/>
		  <div onAnimationEnd={(e)=>{e.target.classList.remove("button-overlay-animated")}} onMouseDown={(e)=>{playAnimation(e)}} className="button-overlay"/>
	  </button>
	);
}

function playAnimation(e) {
	e.target.classList.add("button-overlay-animated");
}

