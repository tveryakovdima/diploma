import React from 'react';
import '../styles/ObjectPreview.css';


function ObjectPreview(props) {
	const style = {
		backgroundImage: 'url("' + props.src +  '")'
	};
	return (
		<div className="ObjectPreview" style={style}>
			<div className="hover-placeHolder"></div>
			<div className="object-name">{props.name}</div>
		</div>
	);
}

export default ObjectPreview;
