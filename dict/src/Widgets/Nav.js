/**
 * Created by prg-15 on 14.03.2018.
 */
import React from 'react';
import {NavLink} from 'react-router-dom';


export default function NavMenu(props) {
	return(
	  <div className="left-nav">
	  <ul>
		  {props.items.map(item =>
			<li className="nav-item" key={item.id}>
				<NavLink
				  to={`/${item.id}`}
				  activeClassName="nav-active"
				  className="nav-link"> {item.name}
				</NavLink>
			</li>
		  )}
	  </ul>
	  </div>
	);
}