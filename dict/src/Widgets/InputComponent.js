/**
 * Created by prg-15 on 04.03.2018.
 */
/**
 * Created by prg-15 on 03.03.2018.
 */
import React from 'react';
export default class InputComponent extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		const type = this.props.type;
		const valMessage = this.props.valMessage;
		const inputClassname = valMessage ? 'form-control form-control-invalid ' : "form-control";
		return (
		  <div className="form-group">
			  <input
				className={inputClassname}
				id={this.props.name}
				onChange={(e)=>{this.props.onChange(e)}}
				placeholder = {this.props.name}
				value={this.props.value}
				type={type}
				name={this.props.name}
			  />
			  {
				  <div class="invalid-feedback">{valMessage}</div>
			  }
		  </div>
		);
	}

}