import React from 'react';
import Grid from "react-bootstrap/es/Grid";
import Row from "react-bootstrap/es/Row";
import Col from "react-bootstrap/es/Col";
import './TrainingPage.css';
import {BrowserRouter, Route, Switch, Link} from 'react-router-dom';
import FormControl from "react-bootstrap/es/FormControl";
import {Button} from 'react-bootstrap';
import AppService from "../../Services/AppService";
import App from "../App";

export default class TrainingPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username:props.username,
            dictLength:null,
        }
    }

    componentDidMount() {
       AppService.ApiCall({username:this.state.username},'/get_words_number').then((data)=>{
           console.log(data);
          this.setState({dictLength:data.number});
       });
    }

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/training' render={() => (<FullTraining dictLength = {this.state.dictLength} />)}/>
                    <Route path='/training/:type' render={(rest) =>
                        (<ChosenTraining username={this.state.username}
                                         dictLength={this.state.dictLength}
                                         {...rest}/>)}/>
                </Switch>
            </BrowserRouter>
        );

    }
}

class FullTraining extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            chosenDictionary:'user',
            words:[],
        }

    }

    selectDictionary(e){
        this.setState({chosenDictionary:e.target.value});
    }

    render() {
        return (
            <Grid fluid>
                <Row>
                    <Col mdOffset={2} md={8} sm={8} xs={8} className={'content-container'}>
                            <Row>
                                <Col md={4}>
                                    <FormControl componentClass="select" onChange={(e)=>(this.selectDictionary(e))}>
                                        <option key={0} value={'user'}>Полный словарь</option>
                                        <option key={1} value={'session'}>Словарь сессии</option>
                                    </FormControl>
                                </Col>
                            </Row>

                            <TrainingCard url={'word-translation'} dictLength={this.props.dictLength} name={'Слово-перевод'}/>
                            <TrainingCard url={'translation-word'} dictLength={this.props.dictLength} name={'Перевод-слово'}/>
                            <TrainingCard url={'word-constructor'} dictLength={this.props.dictLength} name={'Конструктор слов'}/>
                            <TrainingCard url={'word-cards'} dictLength={this.props.dictLength} name={'Карточки слов'}/>
                        </Col>

                </Row>
            </Grid>
        );
    }
}

const TrainingCard = ((props) => {
    let {url, name, dictLength} = props;

    return (
        <Link to={`/training/${url}`} activeClassName="active">
            <Col md={6}>
                <div className={'training-card'}>
                    <div className={'card-inner'}>
                        <span className={'card-title'}>{name}</span>
                        <span className={'word-count'}>{dictLength} слов в словаре</span>
                    </div>
                    <div className={'learn-button-overlay'}>
                        <Button className={'learn-button'} bsStyle="primary">Изучить</Button>
                    </div>
                </div>
            </Col>
        </Link>
    );
});

class ChosenTraining extends React.Component {
    constructor(props){
        super(props);
        this.state = {
           username:props.username,
            type:props.match.params.type,
            user:null,
            currentWord:0,
            dictLength:props.dictLength,
            isLast:props.dictLength === 1,
            pressed:false,
        };

        this.handleSelect = this.handleSelect.bind(this);
    }

    componentDidMount(){
        AppService.ApiCall({username:this.state.username},'/get_user').then((data)=>{
            this.setState({user:data.user});
        });
    }

    nextItem(){
        const curItem = this.state.currentWord;
        const newItem = curItem + 1;
        if (newItem + 1 < this.state.dictLength){
            this.setState({currentWord:newItem});
        }
        else if (newItem + 1 === this.state.dictLength){
            this.setState({isLast:true});
            this.setState({currentWord:newItem});
        }

        this.setState({pressed:false})
    }

    finish(){

    }

    handleSelect(){
        this.setState({pressed:true});
        console.log("tut");
    }

    getRandomTranslations(){
        return ['сыр','машина','компьютер','аптека'];
    }

    render() {
        let wordItems = null;
        let currentItem = null;
        if (this.state.user) {
            wordItems = this.state.user.words.map((word) => (
                <WordTranslation word={word.word}
                                 key={word.word}
                                 translation={word.translation}
                                 random={this.getRandomTranslations()}
                                 pressed={this.state.pressed}
                                 onSelect={this.handleSelect}
                />
            ));
            currentItem = wordItems[this.state.currentWord];
            console.log(currentItem);
        }

        const button = this.state.isLast ?
            <Button onClick={()=>(this.finish())} bsStyle={'primary'}>Завершить</Button> :
            <Button onClick={()=>(this.nextItem())} bsStyle={'primary'}>Дальше</Button>;
        return (
            <Grid fluid>
                <Row>
                    <Col  mdOffset={2} md={8} sm={8} xs={8} className={'content-container wordTraining-container'}>
                        <Row style={{'height':'80%'}}>
                            {currentItem && currentItem}
                        </Row>
                        <Row>
                            <Col mdOffset={8} smOffset={8} xsOffset={8} md={3} sm={3} xs={3}>
                                {button}
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Grid>
        )
    }
}

class WordTranslation extends React.Component{
    constructor(props){
        super(props);
        this.state={
            pressed:props.pressed,
            variants: [props.translation, ...props.random],
            translation:props.translation,
        };
        this.handleSelect = this.handleSelect.bind(this);
    }

    componentDidMount(){
        let variants = this.state.variants;
        variants = this.shuffle(variants);
        this.setState({variants});
    }

    shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }

    handleSelect(){
        this.props.onSelect();
    }


    render(){
        let variants = this.state.variants;
        let varComponents = variants.map((t)=>{
            return (<CustomButton
                onClick={this.handleSelect}
                translation={t} bsSize="large"
                rightAnswer={this.state.translation}
                pressed={this.props.pressed}
                block
            />);
        });
        return(
            <Col className={'wt-inner'}>
                <Col md={6}>
                    <div className={'training-widget'}>
                        <span className={'word'}>{this.props.word}</span>
                    </div>
                </Col>
                <Col md={6}>
                    {varComponents}
                </Col>
            </Col>
        );
    }
}

class CustomButton extends React.Component{
    constructor(props){
        super(props);
        this.state = ({
            wrong:false,
            right:false,
        });
    }

    handleSelect(t){
        console.log(t);
        if (t === this.props.rightAnswer){
            this.setState({right:true});
        }

        if (t !== this.props.rightAnswer){
            this.setState({wrong:true});
        }

        this.props.onClick();
    }
    render(){
        const translation = this.props.translation;
        let bsStyle = 'default';
        if (this.state.wrong) bsStyle = 'danger';
        if (this.state.right) bsStyle = 'success';
        if (this.props.pressed && this.props.rightAnswer === translation) bsStyle = 'success';
        return(
            <Button onClick={()=>(this.handleSelect(translation))}
                    bsSize="large"
                    block
                    bsStyle={bsStyle}
                    disabled={this.props.pressed}
            >
                {translation}
            </Button>
        )
    }
}