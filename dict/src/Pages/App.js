/**
 * Created by prg-15 on 03.03.2018.
 */
import React from 'react';
import AppService from '../Services/AppService'
import LoginPage from './LoginPage'
import RegisterPage from './RegisterPage';
import ObjectPage from './ObjectPage';
import AboutPage from './AboutPage';
import IconButton from '../Widgets/IconButton';
import {Switch, Route, Redirect, NavLink, BrowserRouter as Router} from 'react-router-dom';
import '../styles/App.css';

export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {loggedUser: ""};
	}

	componentWillMount() {
		AppService.ApiCall({}, '/user/status').then((data) => {
			if (data.authenticated) {
				this.setState({loggedUser: data.user.username});
			}
		});
	}

	onHandleLogin = (e) => {
		const username = e.target.username.value;
		const password = e.target.password.value;
		if (AppService.validateUsername(username)) {
			AppService.ApiCall({username: username, password: password}, '/login').then((data) => {
				if (data.hasOwnProperty("errors")) {
					this.onHandleErrors(data.errors);
				}
				else if (data.status == "success") {
					this.setState({loggedUser: username});
				}
			});

		}
	};

	onHandleRegister = (username, password) => {
		if (AppService.validateUsername(username)) {
			AppService.ApiCall({username: username, password: password}, '/login').then((data) => {
				console.log(data);
				if (data.status == "success") {
					this.setState({loggedUser: username});
				}
			});

		}
	};

	onHandleLogOut() {
		AppService.ApiCall({}, '/logout').then((data) => {
			console.log(data);
            this.setState({loggedUser: ""});
		});

	}

	render() {
		let startPage = null;
		let className = "page-wrapper";
		return (
		  <Router>
			  <div>
				  <Header
					username={this.state.loggedUser}
					onLogout={() => {
						this.onHandleLogOut()
					}}
				  />
				  <div className="page-wrapper">
					  <Switch>
						  <Route exact path="/login"
								 render={(e) => (
								 	this.state.loggedUser ? (<Redirect to='/'/>) :
										(<LoginPage onSubmit={e => this.onHandleLogin(e)}/>))}/>
						  <Route exact path="/register" render={(e) => (
						  	this.state.loggedUser ? (<Redirect to='/'/>) :
								(<RegisterPage onSubmit={(username, password) => this.onHandleRegister(username, password)}/>))}/>
						  <Route exact path="/object" component={ObjectPage}/>
						  <Route path="/" component={AboutPage}/>
					  </Switch>
				  </div>
			  </div>
		  </Router>
		);
	}
}

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state = {currentPage: 3};
	}

	render() {
		const username = this.props.username;
		let headerButtons = null;
		if (!username) {
			headerButtons =
			  <ul className="nav right-menu vertical-center">
				  <li>
					  <NavLink
						className="menu-link"
						to='/login'
						activeClassName="menu-link-active"
					  >Login</NavLink>
				  </li>
				  <li>
					  <NavLink
						className="menu-link"
						to='/register'
						activeClassName="menu-link-active"
					  >Register</NavLink>
				  </li>
			  </ul>
		}
		else {
			headerButtons =
			  <ul className="navbar-nav mr-auto">
				  <li><h3>{username}</h3></li>
				  <li>
					  <button className="btn btn-primary" onClick={this.props.onLogout}>Logout</button>
				  </li>
			  </ul>
		}

		return (
		  <header>
			  <div className="vertical-center">
				  <IconButton onClick={()=>{showNav()}} src="/icons/ic_menu_36px.svg"/>
			  </div>
			  <div className="logo vertical-center">
				  <img className="logo-img" src="/images/logo_min_white.png"/>
			  </div>
			  <ul className="nav vertical-center">
				  <li>
					  <NavLink className="menu-link" exact to="/" activeClassName="menu-link-active">About
					  </NavLink>
				  </li>
				  <li>
					  <NavLink className="menu-link" to="/object" activeClassName="menu-link-active">Objects
					  </NavLink>
				  </li>
			  </ul>
			  {headerButtons}
		  </header>
		);
	}
}

function showNav() {

}







