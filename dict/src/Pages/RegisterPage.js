/**
 * Created by prg-15 on 03.03.2018.
 */
import React from 'react';
import {Redirect} from 'react-router-dom'
import InputComponent from '../Widgets/InputComponent';
import AppService from '../Services/AppService';
export default class RegisterPage extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			username:"",
			email:"",
			password:"",
			password_confirm:"",

			usernameValid:"",
			emailValid:"",
			passwordValid:"",
			password_confirmValid:"",

			dataAccepted:false,
			loggedIn:false,
		};
	}

	onHandleInput(e,name){
		this.setState({[name+'Valid']:""});
		this.setState({[name]:e.target.value});
	}

	handleSubmit(e) {
        const formValidation = this.validateForm();
        console.log(formValidation);
        if (Object.keys(formValidation.errors).length != 0) {
            this.onHandleErrors(formValidation.errors);
        }
        else {
            AppService.ApiCall({
                    username: this.state.username,
                    email: this.state.email,
                    password: this.state.password,
                    password_confirm: this.state.password_confirm,
                },
                "/register").then((data) => {
                console.log(data);
                if (data.hasOwnProperty("errors")) {
                    this.onHandleErrors(data.errors);
                }

                else if (data.status == "success") {
                    this.setState({dataAccepted: true});
                    this.handleLogin(e);
                }

            });
        }
		e.preventDefault();
	}

	validateForm(){
        let errorObj = {errors:{}};
        let usernameError = AppService.validateUsername(this.state.username);
        let passwordError = AppService.validatePassword(this.state.password);
        let passwordConfirmError = this.state.password == this.state.password_confirm ? "" :
			"Пароли должны совпадать";
        if (usernameError)
        {
			errorObj.errors.username = usernameError;
        }
        if (passwordError) {
			errorObj.errors.password = passwordError;
        }

        if (passwordConfirmError){
        	errorObj.errors.password_confirm = passwordConfirmError;
		}

        return errorObj;

	}


	handleLogin(e){
		const username = this.state.username;
		const password = this.state.password;
		this.props.onSubmit(username, password);
		e.preventDefault();
	}

	onHandleErrors(errorObj){
		for (let item in errorObj){
			let errorMsg = errorObj[item];
			if (errorMsg){
				let key = item + "Valid";
				this.setState({[key]:errorMsg});
			}
		}
	}

	render(){
		const dataAccepted = this.state.dataAccepted;
		let content = null;
		let header = "";

			header = <h2>Register</h2>;
			content =  <form onSubmit = {e => {
				this.handleSubmit(e)
			}}>
				<InputComponent
				  value={this.state.username}
				  onChange={(e)=>{this.onHandleInput(e,"username")}}
				  type="username"
				  valMessage={this.state.usernameValid}
				  name="username"
				/>

				<InputComponent
				  value={this.state.email}
				  onChange={(e)=>{this.onHandleInput(e,"email")}}
				  type="email"
				  valMessage={this.state.emailValid}
				  name="email"
				/>


				<InputComponent
				  value={this.state.password}
				  onChange={(e)=>{this.onHandleInput(e,"password")}}
				  type="password"
				  valMessage={this.state.passwordValid}
				  name="password"
				/>

				<InputComponent
				  value={this.state.password_confirm}
				  onChange={(e)=>{this.onHandleInput(e,"password_confirm")}}
				  type="password"
				  valMessage={this.state.password_confirmValid}
				  name="password_confirm"
				/>

				<button className="submit-button" type="submit">Register</button>
			</form>


		return(
		  <div className="login-wrapper">
			  <div className="form-wrapper">
				  {header}
				  {content}
			  </div>
		  </div>
		);
	}

}