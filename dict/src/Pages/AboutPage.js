
import React from 'react';
import ObjectPreview from '../Widgets/ObjectPreview';
import {Switch, Route, Link, Redirect, NavLink, BrowserRouter as Router} from 'react-router-dom';
import AppService from '../Services/AppService';
import NavMenu from '../Widgets/Nav';

export default class GridPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {curImagesNum:9, items:[], images:[], total:0};

	}

	componentWillMount(){
		const items = AppService.getAboutArticles();
		const images = items.slice(0,9);
		const total = items.length;
		this.setState({items, images, total});
	}

	componentDidMount() {
		document.addEventListener('scroll', this.handleScroll.bind(this));

	}

	componentWillUnmount() {
		document.removeEventListener('scroll', this.handleScroll);
	}

	handleScroll(event) {
		if (this.state.images >= this.state.items) return;
		console.log(event.target.scrollingElement.scrollTop, event.target.scrollingElement.offsetHeight, event.target.scrollingElement.scrollHeight);


		const scrollBottom =  event.target.scrollingElement.offsetHeight == event.target.scrollingElement.scrollHeight;
			if (scrollBottom) {
				this.getArticles();
			}
		}

	getArticles(){
		let end = 0;
		const items = this.state.items;
		const curImagesNum = this.state.curImagesNum;
		if (this.state.total < curImagesNum + 3){
			end = this.state.total;
		}

		else {
			end = curImagesNum + 3;
		}

		const newImages = items.slice(0, end);
		this.setState({images:newImages, curImagesNum:curImagesNum+3});
	}

	render() {
		const items = AppService.getAboutArticles();
		return (
		  <Router>
			  <div className="About-page">
				  <div className="grid-wrapper">
					  <NavMenu items={items}/>
					  <Switch>
						  <Route exact path="/" render={() => (<FullPage ons={e=>{this.handleScroll(e)}} items={this.state.images} />)}/>
						  <Route exact path="/:id" component={Article}/>
					  </Switch>
				  </div>

			  </div>
		  </Router>
		);
	}
}


function FullPage(props) {
	return (
	  <div className="LayoutGrid">
		  {props.items.map(item =>
			<Link to={`/${item.id}`}><ObjectPreview key={item.id} name={item.name} src={item.src}/></Link>
		  )}
	  </div>
	);
}

function Article(props) {
	const article = AppService.getAboutArticle(props.match.params.id);
	return (
	  <div className="article-wrapper">
		  <h2>{article.name}</h2>
		  {article.content}</div>
	);
}


