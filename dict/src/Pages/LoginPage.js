/**
 * Created by prg-15 on 03.03.2018.
 */
import React from 'react';
import InputComponent from '../Widgets/InputComponent';
import AppService from '../Services/AppService';




export default class LoginPage extends React.Component{
	constructor(props){
		super(props);
		this.state = {username:"", password:"",usernameValid:"",passwordValid:""};
	}

    componentWillReceiveProps(nexProps){
		if (this.props != nexProps){
			if (nexProps.checked) this.setState({usernameValid:nexProps.checked, passwordValid:true});
		}
		else {
            this.setState({usernameValid:nexProps.checked});
		}
	}

	handleSubmit(e) {
		this.props.onSubmit(e);
		e.preventDefault();
	}

	render(){
		const username = this.state.username;
		const password = this.state.password;
		const check = this.props.checked;

		return (
		  <div className="login-wrapper">
			  <div className="form-wrapper">
				  <h2>Please Login</h2>
				  <form
					onSubmit = {e => this.handleSubmit(e)}

				  >
					  <InputComponent
						value={username}
						onChange={(e)=>{this.onHandleUsername(e)}}
						type="username"
						valMessage={this.state.usernameValid}
						name="username"
					  />
					  <InputComponent
						value={password}
						onChange={(e)=>{this.onHandlePassword(e)}}
						type="password"
						valMessage={this.state.passwordValid}
						name="password"
					  />
					  <button className="submit-button" type="submit">Login</button>
				  </form>
			  </div>
		  </div>

		);
	}

	onHandleUsername(event){
		const username = event.target.value;
		const message = this.validateUsername(username);
		this.setState({username:username, usernameValid:message});

	}

	onHandlePassword(event){
		const password = event.target.value;
		const message = this.validatePassword(password);
		this.setState({password:password, passwordValid:message});

	}



	validateUsername(username){
		if (username.length<6){
			return "Length must be > 6";
		}

		return "";
	}

	validatePassword(password){
		if (password.length<6){
			return "Length must be > 6";
		}

		return "";
	}
}

