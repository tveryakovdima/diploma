import React from 'react';
import {Route, Link, NavLink, BrowserRouter} from 'react-router-dom';
import '../styles/App.css';
import '../styles/ObjectPage.css';
import AppService from "../Services/AppService";

export default function ObjectPage() {
	return (
		<BrowserRouter>
			<div>
				<ul>
					{
						AppService.getAboutArticles().map(o => (
							<li key={o.id}>
								<NavLink
									to={`/object/${o.id}`}
									activeStyle={{
										fontWeight: 'bold',
										color: 'red'
									}}>
									{o.name}
								</NavLink>
							</li>
						))
					}
				</ul>
				<Route path='/object/:id' component={ObjectView}/>
			</div>
		</BrowserRouter>
	);
}

function ObjectView(props) {
	const object = AppService.getAboutArticle(parseInt(props.match.params.id, 10));
	if (!object) return <h1>Oops, something went wrong...</h1>;
	return (
		<div>
			<h3>{object.name}</h3>
			<Link to='/object'>Back</Link>
		</div>
	);
}
