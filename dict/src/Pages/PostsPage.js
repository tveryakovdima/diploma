import React from 'react';
import {Grid, Row, Col, Navbar, Nav, NavDropdown, NavItem, FormGroup, FormControl, Button, Glyphicon, MenuItem, Popover, OverlayTrigger, Overlay} from 'react-bootstrap';
import {Switch, Route, Link} from 'react-router-dom';
import AppService from "../Services/AppService";
import App from "./App";
import ListGroup from "react-bootstrap/es/ListGroup";
import ListGroupItem from "react-bootstrap/es/ListGroupItem";
import Alert from "react-bootstrap/es/Alert";

export default class PostsPage extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        const name = this.props.match.params.category;
        const username = this.props.username;
        const url = '/materials/:' + name;
        return(
            <Grid fluid>
                <Switch>
                    <Route exact path={url} render={(rest)=>(<FullPosts name={name} username={username}{...rest} />)}/>
                    <Route path={url + '/:id'} render={(rest)=>(<Post username={username} {...rest}/>)}/>
                </Switch>
            </Grid>
        );
    }
}

class FullPosts extends React.Component{
    constructor(props){
        super(props);
        this.state =
            {
                posts:[],
                chosenSort:0,
                sortType:[
                    {eventKey:0, action:'По объему'},
                    {eventKey:1, action:'По времени'}
                ]
            }
    }

    componentDidMount(){
        AppService.ApiCall({'name':this.props.name},'/get_posts').then((data)=>{
            console.log('data',data);
            this.setState({posts:data});
        });
    }

    onDopdownSelect(e){
        this.setState({chosenSort:parseInt(e)})
    }

    render() {
        const posts = this.state.posts.map((post)=>
            <Row className="show-grid post-row">
                <Col md={12} sm={12} xs={12} className='post-container'>

                        <Col md={1} sm={2} xs={1}>
                            <Glyphicon glyph="book"/>
                        </Col>
                        <Col md={10} sm={10} xs={11}>
                            <Link className='post-link' to={'/materials/'+this.props.name+'/' + post.id}>
                                <div className='post-title'>{post.title}</div>
                            </Link>
                        </Col>
                </Col>
            </Row>
        );

        const dropDownItems = this.state.sortType.map((item)=>
            <MenuItem eventKey={item.eventKey}>{item.action}</MenuItem>
        );
        return (
            <Grid fluid>
                    <Row className='show-grid'>
                        <Col md={12} xm={12} xsHidden>
                        <Navbar fluid>
                            <Col md={8} sm={8}>
                                <Navbar.Header>
                                    <Navbar.Brand>
                                        <span>{this.props.name}</span>
                                    </Navbar.Brand>
                                    <Navbar.Toggle />
                                </Navbar.Header>
                                <Navbar.Collapse>
                                    <Navbar.Form pullLeft>
                                        <FormGroup>
                                            <FormControl type="text" placeholder="Search" />
                                        </FormGroup>{' '}
                                    </Navbar.Form>
                                </Navbar.Collapse>
                            </Col>
                            <Col md={4} sm={4}>
                                <Nav onSelect={(e)=>{this.onDopdownSelect(e)}}>
                                    <NavDropdown
                                        title={this.state.sortType[this.state.chosenSort].action}
                                        id={'sort-drop-down'}
                                    >
                                        {dropDownItems}
                                    </NavDropdown>
                                </Nav>
                            </Col>
                        </Navbar>

                    </Col>
                    </Row>
                <Grid className='posts-container well' fluid>
                    {posts}
                    {!posts.length && <span>No posts yet</span>}
                </Grid>
            </Grid>
        );
    }
}

class Post extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            post:null,postWords:[],
            chosenWord:null, username:props.username,
            showError:false, error:{}
        };
        this.onChooseTranslation = this.onChooseTranslation.bind(this);
        this.handleWordClick = this.handleWordClick.bind(this);
        this.showError = this.showError.bind(this);
        this.hideError = this.hideError.bind(this);
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        console.log('post mounted');
        AppService.ApiCall({'id':id},'/get_post').then((data)=>{
           console.log(data);
           this.setState({post:data});
           const regExp = /[a-z]+\W*/gi;
           this.setState({postWords:data.body.match(regExp)})
            console.log(data.body.match(regExp));
        });
    }

    showError(){
        this.setState({showError:true});
    }

    hideError(){
        this.setState({showError:false});
    }

    onChooseTranslation(translation){
        const {chosenWord, username} = this.state;
        if (!username){
            this.setState({
                showError:true,
                error:{header:"Вы не авторизированны", message:"Пожалуйста, войдите в свой аккаунт, чтобы добавить перевод слова"}
            });
            return;
        }
        if (chosenWord){
            AppService.ApiCall({word:chosenWord, translation:translation, username:username},"/add_word").then((data)=>{
               console.log('translation added',data);
            });
        }
    }

    handleWordClick(word){
        this.setState({chosenWord:word});
    }

    render(){
        const wordItems = this.state.postWords.map((word)=>
            <Word
                word = {word}
                onClick={this.handleWordClick}
                onChooseTranslation={this.onChooseTranslation}
            />
        );
        return(
            <div>
                {this.state.post && <div>{wordItems}</div>}
                {
                    this.state.showError && <UserAlert
                    show={this.state.showError}
                    error={this.state.error}
                    onShow={this.showError}
                    onHide={this.hideError}
                />}
            </div>
        )
    }



}

class Word extends React.Component{
    constructor(props){
        super(props);
        this.state={
            translations:[]
        };
        this.wordRegExp = /[a-z]+/i;
        this.word = props.word.match(this.wordRegExp)[0]
    }

    componentDidMount(){
        console.log('this.word',this.word);
        AppService.ApiCall({word:this.word},'/get_translations').then((data)=>{
           this.setState({translations:data.translations});
        });
    }

    render() {
        const translations = this.state.translations.map((t)=>
                <div
                    className={'translation'}
                    key={t}
                    onClick={()=>(this.props.onChooseTranslation(t))}
                >
                    {t}
                </div>
        );

        const WordPopOver = (
            <Popover id="popover-trigger-click-root-close" title={this.word}>
               <div className={'translation-container'}>
                   {translations}
               </div>
            </Popover>
        );

        return (
            <OverlayTrigger
                trigger='click'
                rootClose
                placement='bottom'
                overlay={WordPopOver}
                target={this}
            >
                 <span
                     onClick={(e) => {
                         this.props.onClick(this.word)
                     }}
                     className='word-item'>
                     {this.props.word}
                 </span>
            </OverlayTrigger>
        );
    }
}

class UserAlert extends React.Component{
    constructor(props){
        super(props);
    }


    render(){
            return(
                <Alert bsStyle={'warning'} onDismiss={()=>(this.props.onHide())}>
                    <h3> {this.props.error.header}</h3>
                    <p>{this.props.error.message}</p>
                    <p>
                        <Button onClick={()=>(this.props.onHide())}>Hide Alert</Button>
                    </p>
                </Alert>
            );
    }
}

