import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap';
import {Switch, Link, Route} from 'react-router-dom';
import AppService from '../Services/AppService';
import PostsPage from './PostsPage';
import ObjectPreview from '../Widgets/ObjectPreview';

const categoryImg = '/images/learnEnglish.jpg';

export default function MaterialPage(props){
        const {username} = props;
        return(
            <Switch>
                <Route exact path='/materials' component={MaterialList}/>
                <Route path='/materials/:category' render={(rest)=>(<PostsPage username={username}{...rest}/>)}/>
            </Switch>
        );
}

class MaterialList extends React.Component{
    constructor(props){
        super(props);
        this.state = {categories:[]};
    }

    componentDidMount(){
        console.log(this.state);
        AppService.ApiCall({},'/categories').then((data)=>{
            console.log(data);
           this.setState({categories:data.categories});
        });
    }
    render(){
        const categories = this.state.categories.map((category)=>
            <Col className="category-wrapper" md={6} sm={6} xs={12}>
                <CategoryPreview src ={category.src} name={category.name}/>
            </Col>
        );

        return(
            <Row className='show-grid'>
                {categories}
            </Row>
        );
        }

}


function CategoryPreview(props){
    const src = props.src ? props.src : categoryImg;
    const path = '/materials/'+props.name;
    return (
        <Link to={path}>
            <ObjectPreview key = {props.name} src={src} name={props.name}/>
        </Link>
    );

}