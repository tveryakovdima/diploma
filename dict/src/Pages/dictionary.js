import React from 'react';
import {Col, Row, Grid, Navbar, Nav, NavItem} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';

import LoginPage from './LoginPage'
import RegisterPage from './RegisterPage';
import MaterialPage from './MaterialPage';
import TrainingPage from './TrainingPage/TrainingPage';


import AppService from "../Services/AppService";

export default class Dictionary extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            loggedUser:"",
            loginCheck:""
        };
    }

    onHandleLogin = (e) => {
        const username = e.target.username.value;
        const password = e.target.password.value;
        const loginError = AppService.validateUsername(username);
        if (!loginError) {
            AppService.ApiCall({username: username, password: password}, '/login').then((data) => {
                if (data.hasOwnProperty("error")) {
                    this.setState({loginCheck:data.error.username})
                }
                else if (data.status == "success") {
                    this.setState({loggedUser: username});
                }
            });

        }

        else{
            this.setState({loginCheck:loginError});
        }
    };

    onHandleLogOut() {
        AppService.ApiCall({}, '/logout').then((data) => {
            console.log(data);
            this.setState({loggedUser: ""});
        });

    }

    onHandleRegister = (username, password) => {
            AppService.ApiCall({username: username, password: password}, '/login').then((data) => {
                console.log(data);
                if (data.status == "success") {
                    this.setState({loggedUser: username});
                }
            });

    };

    render(){
        return(
            <Grid fluid>
                <Router>
                <Row className="show-grid">
                    <Header loggedUser={this.state.loggedUser} handleLogout={()=>{this.onHandleLogOut()}}/>
                    <Col className={'base-container'} md={10} mdOffset={1} sm={8} smOffset={2} xs={12}>
                        <Switch>
                            <Route exact path="/login"
                                   render={() => (
                                       this.state.loggedUser ? (<Redirect to='/'/>) :
                                           (<LoginPage checked = {this.state.loginCheck} onSubmit={e => this.onHandleLogin(e)}/>))}/>
                            <Route exact path="/register" render={() => (
                                this.state.loggedUser ? (<Redirect to='/'/>) :
                                    (<RegisterPage onSubmit={(username, password) => this.onHandleRegister(username, password)}/>))}/>
                            <Route path="/materials" render={()=> <MaterialPage username={this.state.loggedUser}/>}/>
                            <Route path="/training" render={()=> (
                                !this.state.loggedUser ? (<Redirect to='/login'/>) :
                                <TrainingPage username={this.state.loggedUser}/>)}/>
                        </Switch>
                    </Col>
                </Row>
                </Router>
            </Grid>
        )
    }
}

function Header(props){
    const username = props.loggedUser;
    const rightNav = !username ?
        <Nav clasName='login-nav'>
            <LinkContainer to='/login' activeClassName='active-nav-link'>
                <NavItem>Войти</NavItem>
            </LinkContainer>
            <LinkContainer to='/register' activeClassName='active-nav-link'>
                <NavItem>Регистрация</NavItem>
            </LinkContainer>
        </Nav> :
        <Nav>
            <LinkContainer to='/user' activeClassName='active-nav-link'>
                <NavItem>{username}</NavItem>
            </LinkContainer>
            <NavItem>
                <span onClick={()=>{props.handleLogout()}}>Выйти</span>
            </NavItem>
        </Nav>

    return (
        <Col className='sticky-top'>
            <Navbar fluid collapseOnSelect bsStyle='inverse'>
                <Col md={1} sm={1}>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <LinkContainer to='/'>
                                <a href='/'>Dictionary</a>
                            </LinkContainer>
                        </Navbar.Brand>
                        <Navbar.Toggle/>
                    </Navbar.Header>
                </Col>
                <Navbar.Collapse>
                    <Col md={4} sm={6}>
                        <Nav>
                            <LinkContainer className='nav-link' to='/materials' activeClassName='active-nav-link'>
                                <NavItem>Материалы</NavItem>
                            </LinkContainer>
                            <LinkContainer to='/training' activeClassName='active-nav-link'>
                                <NavItem>Тренировки</NavItem>
                            </LinkContainer>
                            <LinkContainer to='/tests' activeClassName='active-nav-link'>
                                <NavItem>Тесты</NavItem>
                            </LinkContainer>
                        </Nav>
                    </Col>
                    <Col md={3} sm={4} mdOffset={4} smOffset={1}>
                        {rightNav}
                    </Col>
                </Navbar.Collapse>
            </Navbar>
        </Col>
    );
}