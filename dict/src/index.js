/**
 * Created by prg-15 on 03.03.2018.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './Pages/App';
import Dictionary from './Pages/dictionary';

const object = {
	name: "Object name",
	properties: {
		width: 100,
		height: 200
	}
};


ReactDOM.render(
	<Dictionary/>,
	document.getElementById('root')
);