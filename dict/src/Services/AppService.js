/**
 * Created by prg-15 on 03.03.2018.
 */
import $ from 'jquery';
import resolve from 'jquery';
import reject from 'jquery';


export default class AppService {
	static ApiCall(_data, url, count=0) {
		return new Promise((resolve, reject) => {
			$.ajax({
				type: 'POST',
				url:url,
				data:JSON.stringify(_data),
				success(dataGet) {
					if ((dataGet.hasOwnProperty('error')) && (dataGet.error.code === 22)) {
						count = count + 1;
						if (count < 10)
							setTimeout(function () {
								this.ApiCall(_data, url, count).then((dataGet) => {
									resolve(dataGet);
								}).catch((err) => {
									reject(err);
								});
							}, 500);
					} else {
						resolve(dataGet);
					}
				},
				error(err) {
					reject(`Error! Code: ${err.status} - ${err.statusText}`);
				}
			});
		});

	}

	static validateUsername(username){
		let check = /[\s@#$%^&*()+-<>]/;
		if (!check.test(username) && username.length > 5){
			return "";
		}

		return "Неподходящее имя";

	}

	static validatePassword(password){
		if (password.lenght < 5){
			return "Cлишком короткий пароль"
		}

		else return "";
	}

	static getAboutArticle(id){
		return AboutPageItems[id];
	}

	static getAboutArticles(){
		return AboutPageItems.slice();
	}



}

const AboutPageItems = [
	{id:0, name: "Basic information", src: "http://www.cruzo.net/user/images/k/6befd660cb87fdaec9c9c6908a10523f_136.jpg", content:"Basic information content"},
	{id:1, name: "What we have", src: "https://pbs.twimg.com/media/DNLAPiXV4AAggws.jpg", content:"What we have content"},
	{id:2, name: "What we offer", src: "http://xaxa-net.ru/uploads/posts/2015-03/1425206375_kadry-kotorye-navevayut-poshlye-mysli-201503011.jpg", content: "What we offer content"},
	{id:3, name: "Basic information", src: "http://www.cruzo.net/user/images/k/6befd660cb87fdaec9c9c6908a10523f_136.jpg", content:"Basic information content"},
	{id:4, name: "What we have", src: "https://pbs.twimg.com/media/DNLAPiXV4AAggws.jpg", content:"What we have content"},
	{id:5, name: "What we offer", src: "http://xaxa-net.ru/uploads/posts/2015-03/1425206375_kadry-kotorye-navevayut-poshlye-mysli-201503011.jpg", content: "What we offer content"},
	{id:6, name: "Basic information", src: "http://www.cruzo.net/user/images/k/6befd660cb87fdaec9c9c6908a10523f_136.jpg", content:"Basic information content"},
	{id:7, name: "What we have", src: "https://pbs.twimg.com/media/DNLAPiXV4AAggws.jpg", content:"What we have content"},
	{id:8, name: "What we offer", src: "http://xaxa-net.ru/uploads/posts/2015-03/1425206375_kadry-kotorye-navevayut-poshlye-mysli-201503011.jpg", content: "What we offer content"},
	{id:9, name: "Basic information", src: "http://www.cruzo.net/user/images/k/6befd660cb87fdaec9c9c6908a10523f_136.jpg", content:"Basic information content"},
	{id:10, name: "What we have", src: "https://pbs.twimg.com/media/DNLAPiXV4AAggws.jpg", content:"What we have content"},
	{id:11, name: "What we offer", src: "http://xaxa-net.ru/uploads/posts/2015-03/1425206375_kadry-kotorye-navevayut-poshlye-mysli-201503011.jpg", content: "What we offer content"},

];